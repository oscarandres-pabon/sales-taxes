## PROBLEM TWO: SALES TAXES

Basic sales tax is applicable at a rate of 10% on all goods, except books, food, and medical taxs that are exempt. Import duty is an additional sales tax applicable on all imported goods at a rate of 5%, with no exemptions.

When I purchase items I receive a receipt which lists the name of all the items and their price (including tax), finishing with the total cost of the items, and the total amounts of sales taxes paid. The rounding rules for sales tax are that for a tax rate of n%, a shelf price of p contains (np/100 rounded up to the nearest 0.05) amount of sales tax.

Write an application that prints out the receipt details for these shopping baskets… INPUT:

- **Input 1:** 1 book at 12.49 1 music CD at 14.99 1 chocolate bar at 0.85

- **Input 2:** 1 imported box of chocolates at 10.00 1 imported bottle of perfume at 47.50

- **Input 3:** 1 imported bottle of perfume at 27.99 1 bottle of perfume at 18.99 1 packet of headache pills at 9.75 1 box of imported chocolates at 11.25

### OUTPUT

- **Output 1:** 1 book : 12.49 1 music CD: 16.49 1 chocolate bar: 0.85 Sales Taxes: 1.50 Total: 29.83
 
- **Output 2:** 1 imported box of chocolates: 10.50 1 imported bottle of perfume: 54.65 Sales Taxes: 7.65 Total: 65.15

- **Output 3:** 1 imported bottle of perfume: 32.19 1 bottle of perfume: 20.89 1 packet of headache pills: 9.75 1 imported box of chocolates: 11.85 Sales Taxes: 6.70 Total: 74.68


### OBSERVATIONS
> The price of the  imported chocolate contains an error, the original price is 11.25 and the taxes for this item are 5% (importation tax), so the tax is equal to 0.5625 that means that de nearest 0.05 is 0.55; Adding the 0.55 to the original price es equal to 11.8 not 11.85 as appear in the output.


## GETTING STARTED

1. Clone the repository into a folder
    ```sh
    git clone git@gitlab.com:oscarandres-pabon/sales-taxes.git
    ```
2. Go to the project folder and execute:
    ```sh
    bundle install
    ```
3. Create and populate the database
   ```sh
   rake db:migrate
   rake db:seed
   ```

4.  Execute the server

    ```sh
    rails s
    ```


## TESTING

1. Go to the project folder, and execute
   ```sh
   rspec
   ```

## SERVICES AVAILABLE

### CHECKOUT

- Do checkout (get bill)
    
    Params: 
    ```json
    Method: POST
    Url: /checkout
    params: {"checkout": {"products": [product_id, ....]}}
    ```
  
### PRODUCT
- Show all products
    ``` json
    Method: GET
    Url: /products

    ```
  
-  Create product
    ```json
    Method: POST
    Url: /products
    Accept: application/json
    params: {"product" : {"name": string , "price": float, "description": string, "category_id" : int}}
    ```

- Show all products
    ``` json
    Method: GET
    Url: /products

    ```
- Update Product
  ```json
    Method: PUT
    Accept: application/json
    Url: /products/{product_id}
    params: {"product" : {"name": string , "price": float, "description": string, "category_id" : int}}
    ```

- Show all products
    ``` json
    Method: DELETE
    Url: /products/{product_id}
    ```

### Category
- Show all categories
    ``` json
    Method: GET
    Url: /categories

    ```
  
-  Create product
    ```json
    Method: POST
    Url: /categories
    Accept: application/json
    params: {"category" : {"name": string, "tax_list": [tax_id, .... ]}}
    ```

- Show all categories
    ``` json
    Method: GET
    Url: /categories

    ```
- Update Product
  ```json
    Method: PUT
    Accept: application/json
    Url: /category/{category_id}
    params: {"category" : {"name": string, "tax_list": [tax_id, .... ]}}
    ```

- Show all categories
    ``` json
    Method: DELETE
    Url: /categories/{category_id}
    ```

### Taxes
- Show all taxes
    ``` json
    Method: GET
    Url: /taxes

    ```
  
-  Create tax
    ```json
    Method: POST
    Url: /taxes
    Accept: application/json
    params: {"tax" : {"name": string,"percent" : float, "categories_list": [category_id, ...]}}
    ```

- Show all taxes
    ``` json
    Method: GET
    Url: /taxes

    ```
- Update tax
  ```json
    Method: PUT
    Accept: application/json
    Url: /taxes/{tax_id}
    params: {"tax" : {"name": string,"percent" : float, "categories_list": [category_id, ...]}}
    ```

- Show all taxes
    ``` json
    Method: DELETE
    Url: /taxes/{tax_id}
    ```