Rails.application.routes.draw do
  resources :taxes
  resources :categories
  resources :products
  get 'test/:id', to: "checkout#check_taxes"
  match 'checkout/', to: "checkout#index", via: [:post]
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
