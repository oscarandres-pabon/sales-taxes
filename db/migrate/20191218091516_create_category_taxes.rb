class CreateCategoryTaxes < ActiveRecord::Migration[6.0]
  def self.up
    create_table :categories_taxes, :id => false  do |t|
      t.string :category_id
      t.string :tax_id      
    end
    add_index :categories_taxes, [:category_id, :tax_id]
  end
  def self.down
    drop_table :categories_taxes
  end
end
