# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

taxes = Tax.create([{name: 'Imported', percent: 5},{name: 'VAT', percent: 10}])
categories = Category.create([
    {name:'Books', tax_list: []},{name: 'Food',tax_list: []},
    {name: 'Medical Products',tax_list: []},
    {name:'Imported Books',tax_list: [taxes.first.id]},
    {name: 'Imported Food',tax_list: [taxes.first.id]},
    {name: 'Imported Medical Products',tax_list: [taxes.first.id]},
    {name: 'Other',tax_list: [taxes[1]]},
    {name: "Imported Other",tax_list: [taxes.first.id, taxes.second.id]}
])

products = Product.create([
    {name: 'Book', price: 12.49, description:'', category_id:categories.find{|item| item.name.include?('Books')}.id},
    {name: 'Music CD', price: 14.99, description:'', category_id:categories.find{|item| item.name.include?('Other')}.id},
    {name: 'Chocolate bar', price: 0.85, description:'', category_id:categories.find{|item| item.name.include?('Food')}.id},
    {name: 'Imported box of chocolates', price: 10.00, description:'', category_id:categories.find{|item| item.name.include?('Imported Food')}.id},
    {name: 'Imported bottle of perfume', price:  47.50, description:'', category_id:categories.find{|item| item.name.include?('Imported Other')}.id},
    {name: 'Imported bottle of perfume', price: 27.99, description:'', category_id:categories.find{|item| item.name.include?('Imported Other')}.id},
    {name: 'Bottle of perfume', price: 18.99 , description:'', category_id:categories.find{|item| item.name.include?('Other')}.id},
    {name: 'Packet of headache pills', price: 9.75, description:'', category_id:categories.find{|item| item.name.include?('Medical Products')}.id},
    {name: 'Box of imported chocolates', price: 11.25, description:'', category_id:categories.find{|item| item.name.include?('Imported Food')}.id}
])