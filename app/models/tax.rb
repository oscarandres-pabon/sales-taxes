class Tax < ApplicationRecord
    attribute :categories_list
    validates :name, presence: true
    has_and_belongs_to_many :categories, dependent: :destroy
   
    after_save :update_categories
    after_update :update_categories

    def update_categories
        categories.delete_all
        selected_categories = categories_list.nil? ? [] : Tax.where(id: self.categories_list)        
        selected_categories.each {|tax| self.categories << tax}
   end
         
end
