class Product < ApplicationRecord
    belongs_to :category, foreign_key: 'category_id'    
    validates_presence_of :name, :price, :category_id    
end
