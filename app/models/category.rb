class Category < ApplicationRecord
     attribute :tax_list
     has_many :products
     validates :name, presence: true
     has_and_belongs_to_many :taxes, dependent: :destroy

     after_save :update_taxes
     after_update :update_taxes
     before_destroy :check_for_products
  
     private
     def check_for_products
          if products.count> 0
               self.errors[:base] << "It is not possible to delete this category because has associated products"             
               raise ActiveRecord::Rollback
               return false
          end
     end

     def update_taxes
          taxes.delete_all
          selected_taxes = tax_list.nil? ? [] : Tax.where(id: self.tax_list)        
          selected_taxes.each {|tax| self.taxes << tax}
     end
end
     