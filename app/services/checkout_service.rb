class CheckoutService
  def build_bill(products)
    @product_list = []
    products.each do |productId|
      @product = Product.find_by(id: productId)
      if (@product)
        @taxes = get_taxes(@product.category_id)
        @price = calculate_price(@product.price, @taxes)
        @product_list << @price.merge("name" => @product.name)
      else
        return []
      end
    end
    return get_totals(@product_list).merge("products" => @product_list)
  end

  def get_taxes(id)
    if (!id.is_a?(Integer))
      id.to_i
    end
    @category = Category.find_by(id: id)
    if (@category)
      return  @category.taxes.all
    end
  end

  def round_taxes(productPrice, totalTaxes)
    if productPrice
      return  (((productPrice * totalTaxes) / 10) * 2).round() /20.0
    else
      return 0
    end
  end

  private
 
  def calculate_price(productPrice, taxes)
    @total_taxes = 0
    taxes.each do |tax|
      @total_taxes +=  tax.percent
    end
    @rounded_tax = round_taxes(productPrice, @total_taxes)
    return {"price" => productPrice, "tax" => @rounded_tax, "totalPrice" => (productPrice + @rounded_tax).round(2) }
  end
  # Return Bill totals
  def get_totals(productList)
    @total_taxes = 0
    @total_price_no_taxes = 0
    productList.each do |product|
      @total_taxes += product["tax"]
      @total_price_no_taxes += product["price"]
    end
    return {"totalTaxes" => @total_taxes.round(2), "totalNoTaxes" => @total_price_no_taxes.round(2), "total" => (@total_taxes + @total_price_no_taxes).round(2) }
  end
end