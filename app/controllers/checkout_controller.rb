class CheckoutController < ApplicationController

    def index
      checkout_object = CheckoutService.new
      products = checkout_params[:products]
      if products.empty?
        render json: {message: 'Insert at least one product' }, status: 403
      else        
        @Bill = checkout_object.build_bill(products)
        if @Bill.empty?
          render json: {message: 'Product not found' }, status: :unprocessable_entity
        else
          render json:  @Bill
        end

      end
    end
    def checkout_params
        params.require(:checkout).permit(products: [])
    end
    
end
