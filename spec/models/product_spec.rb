require 'rails_helper'

RSpec.describe Product, type: :model do  

  subject {
    Product.new(name: "Anything", description: "Lorem ipsum", price: 25, category_id: 2)
  }

  context "validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end
  
    it "is not valid without a name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end
  
    it "is not valid without a description" do
      subject.description = nil
      expect(subject).to be_valid
    end
  
    it "is not valid without a price" do
      subject.price = nil
      expect(subject).to_not be_valid
    end
  
    it "is not valid without a category_id" do
      subject.category_id = nil
      expect(subject).to_not be_valid
    end    
  end
  context "asociation" do
    it "belongs to one category" do
      assc = described_class.reflect_on_association(:category)
      expect(assc.macro).to eq :belongs_to
    end
  end
  
end
