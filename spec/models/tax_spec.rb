require 'rails_helper'

RSpec.describe Tax, type: :model do
  subject {
    Tax.new(name: "Anything", percent: 21)
  }
  
  context "validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end
  
    it "is not valid without a name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end
  
    it "is not valid without a percent" do
      subject.percent = nil
      expect(subject).to be_valid
    end
  
    it "is valid without a category_list" do
      subject.categories_list = nil
      expect(subject).to be_valid
    end    
  end
  context "asociation" do
    it "belongs to one category" do
      assc = described_class.reflect_on_association(:categories)
      expect(assc.macro).to eq :has_and_belongs_to_many
    end
  end
  
end
