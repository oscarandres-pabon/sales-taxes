require 'rails_helper'

RSpec.describe Category, type: :model do
  subject {
    Category.new(name: "whatever", tax_list: [1,2])
  }
  
  context "validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end
  
    it "is not valid without a name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end      
    it "is not valid without a tax_list" do
      subject.tax_list = nil
      expect(subject).to be_valid
    end      
  end
  context "asociations" do
    it "has many products" do
      assc = described_class.reflect_on_association(:products)
      expect(assc.macro).to eq :has_many
    end

    it "has many products" do
      assc = described_class.reflect_on_association(:taxes)
      expect(assc.macro).to eq :has_and_belongs_to_many
    end
  end    
  
end
