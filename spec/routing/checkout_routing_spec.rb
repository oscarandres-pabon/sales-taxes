require 'rails_helper'

RSpec.describe 'Routes Spec', type: :routing do 
    describe 'routing to checkout' do
      it 'routes /checkout to checkout#index' do
        expect(post: "/checkout").to route_to(
            controller: "checkout",
            action: "index"
          )
      end      
    end      
end