require 'rails_helper'

RSpec.describe 'Routes Spec', type: :routing do 
    describe 'routing to taxes' do
      it 'routes /taxes to taxes#index' do
        expect(get: "/taxes").to route_to(
            controller: "taxes",
            action: "index"
          )
      end
      it "routes /taxes/1 to taxes#show" do
        expect(get: "/taxes/1").to route_to(
          controller: "taxes",
          action: "show",
          id: "1"
        )
      end     
    
      it "routes /taxes to taxes#create" do
        expect(post: "/taxes").to route_to(
          controller: "taxes",
          action: "create"
        )
      end      
    
      it "routes /taxes/1 to taxes#update" do
        expect(put: "/taxes/1").to route_to(
          controller: "taxes",
          action: "update",
          id: "1"
        )
      end
    
      
    end      
end