require 'rails_helper'

RSpec.describe 'Routes Spec', type: :routing do 
    describe 'routing to products' do
      it 'routes /products to products#index' do
        expect(get: "/products").to route_to(
            controller: "products",
            action: "index"
          )
      end
      it "routes /products/1 to products#show" do
        expect(get: "/products/1").to route_to(
          controller: "products",
          action: "show",
          id: "1"
        )
      end     
    
      it "routes /products to products#create" do
        expect(post: "/products").to route_to(
          controller: "products",
          action: "create"
        )
      end      
    
      it "routes /products/1 to products#update" do
        expect(put: "/products/1").to route_to(
          controller: "products",
          action: "update",
          id: "1"
        )
      end      
    end      
end