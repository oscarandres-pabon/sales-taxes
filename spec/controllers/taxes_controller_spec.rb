require 'rails_helper'

RSpec.describe TaxesController, type: :controller do
  let!(:tax) { create :tax}

  # Get Taxes Case
  describe "GET #index" do
    context "When all the taxes are requested" do
      it "returns a success response" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end
  end

  # Get Tax Case
  describe "GET #show" do
    context "When one tax is requested" do
      it "returns a success response " do
        get :show, params:{id: tax.id}
        expect(response).to have_http_status(:success)
      end
    end
  end

  # New Tax cases
  describe "POST #create" do
    let(:new_tax) {{ "tax" => {"name" => "test-tax", "percent" => 20} }}
    context "Create new tax based on parameters" do
      context "when are valid parameters" do
        it "returns a success response" do
          get :create, params: new_tax
          expect(response).to have_http_status(:success)
          expect(Tax.where(name: "test-tax")).to exist
        end
      end
      context "when are invalid parameters" do
        it "returns an error response" do          
          expect{post(:create, {})}.to raise_error ActionController::ParameterMissing                
        end
       end
    end
  end

  # Edit Tax cases
  describe "PUT #update" do
    let(:attr) {{ :name => "tax_test_edit", :percent => 2 }}
      
    context "Edit tax based on parameters" do
      context "when are valid parameters" do
        it "returns a success response" do
          get :update, params:{id: tax.id, tax: attr}
          expect(response).to have_http_status(:success)
        end
        it "returns a consistency of data response" do
          prev_updated_at = tax.updated_at
          get :update, params:{id: tax.id, tax: attr}
          tax.reload
          expect(tax.updated_at).should_not eql(prev_updated_at)
          expect(tax.name).to eql('tax_test_edit')
        end
      end
      context "when are invalid parameters" do
        it "returns an error response" do          
          expect{post(:create, {})}.to raise_error ActionController::ParameterMissing                
        end
       end
    end


  end

  # Delete cases
  describe " DELETE #destroy" do
    context "Delete tax" do
      it "Returns a success response" do
       get :destroy, params:{id: tax.id}
       expect(response).to have_http_status(204)
     end
     it "Returns was effectively deleted" do
       get :destroy, params:{id: tax.id}
       expect(Tax.where(id: tax.id).first).to  be(nil)
     end     
    end
  end
end
