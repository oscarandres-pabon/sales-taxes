require 'rails_helper'
require 'faker'

RSpec.describe ProductsController, type: :controller do
    let!(:category) { create :category}
    let!(:product) { create :product, category_id: category.id}

    # Get Products Case
    describe "GET #index" do
      context "When all the products are requested" do
        it "returns a success response" do
          get :index
          expect(response).to have_http_status(:success)
        end
      end
    end
  
    # Get Product Case
    describe "GET #show" do
      context "When one product is requested" do
        it "returns a success response" do
          get :show, params:{id: product.id}
          expect(response).to have_http_status(:success)
        end
        it "response with JSON body containing expected Product attributes" do
          hash_body = nil
          get :show, params:{id: product.id}
          expect { hash_body = JSON.parse(response.body).with_indifferent_access }.not_to raise_exception
          expect(hash_body.keys).to match_array(["id", "description", "name", "price", "category_id", "created_at", "updated_at"])
          expect(hash_body).to include({
            "id" => product.id,
            "name" => product.name,
            "description" => product.description,
            "price" => product.price,
            "category_id" => product.category_id
          })
        end
      end
    end
  
    # New Product cases
    describe "POST #create" do
      let(:new_product) {{ "product" => {"name" => "test-product", "price" => 43.2, "descrition" => " lorem lorem ", "category_id": category.id } }}
      context "Create a new product based on parameters" do
        context "when are valid parameters" do
          it "returns a success response" do
            get :create, params: new_product
            expect(response).to have_http_status(:success)
            expect(Product.where(name: "test-product")).to exist        
          end
        end
        context "when are invalid parameters" do
          it "returns an error response" do          
            expect{post(:create, {})}.to raise_error ActionController::ParameterMissing                
          end
         end
      end
    end
  
    # Edit Product cases
    describe "PUT #update" do
      let(:attr) {{ :name => "product_test_edit" }}
      context "Edit product based on parameters" do
        context "when are valid parameters" do
          it "returns a success response" do
            get :update, params:{id: product.id, product: attr}
            expect(response).to have_http_status(:success)
          end
      
          it "returns a consistency of data response" do
            prev_updated_at = product.updated_at
            get :update, params:{id: product.id, product: attr}
            product.reload
            expect(product.updated_at).should_not eql(prev_updated_at)
            expect(product.name).to eql('product_test_edit')
          end
        end
        context "when are invalid parameters" do
          it "returns an error response" do          
            expect{post(:create, {})}.to raise_error ActionController::ParameterMissing                
          end
         end
      end  
    end
  
    # Delete cases
    describe "DELETE #destroy" do
      context "Delete product" do        
        it "Returns a success response" do
            get :destroy, params:{id: product.id}
            expect(response).to have_http_status(204)
        end
        it "Returns was effectively deleted" do
            get :destroy, params:{id: product.id}
            expect(Product.where(id: product.id).first).to  be(nil)
        end      
      end
    end
end
