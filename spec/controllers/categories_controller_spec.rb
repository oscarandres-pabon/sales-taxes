require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
    let!(:category) { create :category}

    # Get Categories Case
    describe "GET #index" do      
      context "When all the categories are requested" do
        it "returns a success response" do
          get :index
          expect(response).to have_http_status(:success)
        end    
      end      
    end
  
    # Get Category Case
    describe "GET #show" do      
      context "When one category is requested" do
        it "returns a success response" do
          get :show, params:{id: category.id}
          expect(response).to have_http_status(:success)
        end                  
      end      
    end
  
    # New Category cases
    describe "POST#create" do
      let(:new_category) {{ "category" => {"name" => "test-category", "taxList" => [2]} }}
      let(:invalid_category) {{ "category" => {"taxList" => []} }}
      
      context "Create new category based on parameters" do
       context "when are valid parameters" do
        it "returns a success response" do
          get :create, params: new_category
          expect(response).to have_http_status(:success)
          expect(Category.where(name: "test-category")).to exist        
        end
       end
       context "when are invalid parameters" do
        it "returns an error response" do          
          expect{post(:create, {})}.to raise_error ActionController::ParameterMissing                
        end
       end
      end
    end
  
    # Edit Category cases
    describe "PUT#update" do
      let(:attr) {{ :name => "category_test_edit" }}
      context "Edit category based on parameters" do
        context "when are valid parameters" do
          it "returns a success response" do
            get :update, params:{id: category.id, category: attr}
            expect(response).to have_http_status(:success)
          end
          it "returns a consistency of data response" do
            prev_updated_at = category.updated_at
            get :update, params:{id: category.id, category: attr}
            category.reload
            expect(category.updated_at).should_not eql(prev_updated_at)
            expect(category.name).to eql('category_test_edit')
          end
        end
        context "when are invalid parameters" do
          it "returns an error response" do          
            expect{post(:create, {})}.to raise_error ActionController::ParameterMissing                
          end
         end
      end  
    end
  
    # Delete cases
    describe "DELETE#destroy" do
      context "Delete category" do
        context "when the category doenst have linked products" do
          it "Returns a success response" do
              get :destroy, params:{id: category.id}
              expect(response).to have_http_status(204)
          end
          it "Returns was effectively deleted" do
              get :destroy, params:{id: category.id}
              expect(Category.where(id: category.id).first).to  be(nil)
          end
        end
        context "when the category has linked products" do
          it ", can not delete category with linked products" do
            create(:product, category_id: category.id )
            get :destroy, params:{id: category.id}
            expect(response).to  have_http_status(:unprocessable_entity)
            expect(Category.where(id: category.id)).to  exist
          end
        end
      end
    end
end
