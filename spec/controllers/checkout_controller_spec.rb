require "rails_helper"
require 'json'

RSpec.describe CheckoutController, type: :controller do

  describe "POST #index" do
    before(:each)  do
      @FIRST_CASE = JSON.parse(file_fixture("first_case.json").read)
      @SECOND_CASE = JSON.parse(file_fixture("second_case.json").read)
      @THIRD_CASE = JSON.parse(file_fixture("third_case.json").read)
    end

    context "Request the checkout with the example products" do
      it "Return first case (No tax product and VAT product)" do
        get :index, params:{checkout: {"products" => [1, 2, 3] } }      
        expect(JSON.parse(response.body)).to eq @FIRST_CASE
      end
      it "Return second case (imported food  and imported product)" do
        get :index, params:{checkout: {"products" => [4,5] } }
        expect(JSON.parse(response.body)).to eq @SECOND_CASE
      end
  
      it "Return second case (imported food  and imported product)" do
        get :index, params:{checkout: {"products" => [6,7,8,9] } }
        expect(JSON.parse(response.body)).to eq @THIRD_CASE
      end
    end
    context "request with empty products param" do
      it "No Existing Product" do
        get :index, params:{checkout: {"products" => [100] } }        
        expect(response).to have_http_status(422)
        expect(JSON.parse(response.body)).to eq( { "message"  => 'Product not found'})
      end
    end
  end
end