require 'rails_helper'

RSpec.describe Product do
    describe "Bill functions" do
      subject { CheckoutService.new }
      let!(:testData) {create :category}

      context "When the list of products is empty" do
        it "return and empty array and 0 as total" do
          expect(subject.build_bill([])).to eq({"total"=> 0, "totalNoTaxes" => 0, "totalTaxes" => 0, "products" => []})
        end
      end
      
      context "#get_taxes" do
        context "The id is  not an integer" do
          it "return Success" do
              @category  = Category.find(8)
              @getTaxesAns = subject.get_taxes("8")
              expect([@getTaxesAns.to_a]).to contain_exactly(@category.taxes.all.to_a)
          end
        end
      
        context "when the id value is a character" do
          it "return null" do
            @getTaxesAns = subject.get_taxes("a")
            expect(@getTaxesAns).to eq(nil)
          end
        end
      end
      context "when the taxes have to be rounded to the nearest 0.05" do
        it "Its not rounding properly #round_taxes" do
          expect(subject.round_taxes(11.3,10)).to eq 1.15
          expect(subject.round_taxes(14.99,10)).to eq 1.5
          expect(subject.round_taxes(27.99 ,15)).to eq 4.2
          expect(subject.round_taxes(10.0,5)).to eq 0.5
        end
      end
      context "when build bill is executed" do
        before(:each)  do
          @FIRST_CASE = JSON.parse(file_fixture("first_case.json").read)
          @SECOND_CASE = JSON.parse(file_fixture("second_case.json").read)
          @THIRD_CASE = JSON.parse(file_fixture("third_case.json").read)
        end
    
        context "Request the checkout with the example products" do
          it "Return first case (No tax product and VAT product)" do            
            p @FIRST_CASE
            p subject.build_bill( [1, 2, 3])
            expect(subject.build_bill( [1, 2, 3])).to eq @FIRST_CASE
          end
          it "Return second case (imported food  and imported product)" do            
            expect(subject.build_bill([4,5])).to eq @SECOND_CASE
          end
      
          it "Return second case (imported food  and imported product)" do            
            expect(subject.build_bill([6,7,8,9])).to eq @THIRD_CASE
          end
        end
        context "request with empty products param" do
          it "No Existing Product" do            
            expect(subject.build_bill([100])).to eq( [])
          end
        end
      end
    end    
end
