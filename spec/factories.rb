require 'faker'

FactoryBot.define do
 

  factory :tax do
    name { Faker::Lorem.word  }
    percent {Faker::Number.number(digits: 2)}
  end

  factory :category do
    name {Faker::Lorem.word }
    
    taxes { |a| [a.association(:tax)] }
    # taxList { |a| [a.association(:tax)] }
  end

  factory :product do
    name { Faker::Lorem.word  }
    price { Faker::Number.decimal(l_digits: 2, r_digits: 3)}
    description { Faker::Lorem.sentence(word_count: 10)}    
    category_id { Faker::Number.between(from: 1, to: 9)}
  end
end